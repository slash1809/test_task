<?php

namespace App\Http\Controllers;

use App\Department;
use App\DepartmentRelationship;
use App\User;
use Illuminate\Http\Request;


class DepartmentsController extends Controller
{
    const PER_PAGE = 4;
    private $path_url;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::select(['id', 'name', 'description', 'logo'])->paginate(self::PER_PAGE)->toArray();
        if ( ! empty($departments['data'])) {
            foreach ($departments['data'] as $key => $department) {
                $departments['data'][$key]['users'] = self::getUsedUsers((int)$department['id'], true);
            }
        }

        return view('departments/main', compact('departments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $department['users']     = User::select(['id', 'name', 'email'])->get()->toArray();
        $department['usedUsers'] = [];

        return view('departments/edit', compact('department'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $department              = Department::where('id', $id)->firstOrFail()->toArray();
        $department['action']    = 'edit';
        $department['users']     = User::select(['id', 'name', 'email'])->get()->toArray();
        $department['usedUsers'] = self::getUsedUsers($id);

        return view('departments/edit', compact('department'));
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function delete(Request $request)
    {
        $request->validate([
          'data.id' => 'required'
        ]);

        if (Department::where('id', (int)$request->data['id'])->delete()) { //
            return Department::paginate(self::PER_PAGE, ['*'], 'page', $request->data['page'])->toArray();
        }

        return false;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function insert(Request $request)
    {
        $request->validate([
          'name' => 'required'
        ]);

        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            $this->path_url = $request->file('logo')->store('app/logo', 'public');
        }

        $department = Department::create([
          'name'        => $request->name,
          'description' => ! empty($request->description) ? $request->description : '',
          'logo'        => $this->path_url,
        ]);

        /**
         * Saving users for new department
         */
        if ( ! empty($request->users) && ! empty($department->id)) {
            DepartmentRelController::setUsers($request, $department->id);
        }

        if ($department->id) {
            return $department->id;
        } else {
            return false;
        }

    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function update(Request $request)
    {
        $request->validate([
          'id'   => 'integer',
          'name' => 'required'
        ]);

        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            $this->path_url = $request->file('logo')->store('app/logo', 'public');
        }

        $user_data = [
          'name' => $request->name,
        ];

        if ( ! empty($request->description)) {
            $user_data['description'] = $request->description;
        }
        if ( ! empty($this->path_url)) {
            $user_data['logo'] = $this->path_url;
        }

        return Department::where('id', $request->id)->update($user_data);

    }

    private static function getUsedUsers(int $id, $all_data = false)
    {
        $department_user = DepartmentRelationship::select('user_id')->where('department_id', $id)->get();
        $department_user = $department_user->pluck('user_id')->toArray();
        if ($all_data) {
            return User::whereIn('id', $department_user)->get()->toArray();
        }

        return $department_user;
    }

}
