<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class UsersController extends Controller
{
    const PER_PAGE = 4;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select(['id', 'name', 'email', 'created_at'])->paginate(self::PER_PAGE)->toArray();

        return view('users/main', compact('users'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('users/edit');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user           = User::select(['id', 'name', 'email'])->where('id', $id)->firstOrFail()->toArray();
        $user['action'] = 'edit';

        return view('users/edit', compact('user'));
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function delete(Request $request)
    {
        $request->validate([
          'data.id' => 'required'
        ]);

        if (User::where('id', (int)$request->data['id'])->delete()) {
            return User::select(['id', 'name', 'email', 'created_at'])->paginate(self::PER_PAGE, ['*'], 'page', $request->data['page'])->toArray();
        } else {
            return false;
        }

    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function insert(Request $request)
    {
        $request->validate([
          'data.name'     => 'required',
          'data.email'    => 'required|email|unique:users,email',
          'data.password' => 'required',
        ]);

        $user = User::create([
          'name'           => $request->data['name'],
          'email'          => $request->data['email'],
          'password'       => bcrypt($request->data['password']),
          'remember_token' => Str::random(10)
        ]);


        if ($user) {
            return $user->id;
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {

        $request->validate([
          'data.id' => 'required|integer'
        ]);

        $user_data = [];
        if ( ! empty($request->data['name'])) {
            $user_data['name'] = $request->data['name'];
        }

        if ( ! empty($request->data['password'])) {
            $user_data['password'] = bcrypt($request->data['password']);
        }

        User::where('id', $request->data['id'])->update($user_data);

    }

}
