<?php

namespace App\Http\Controllers;

use App\DepartmentRelationship;
use Illuminate\Http\Request;

class DepartmentRelController extends Controller
{
    private static $department_id;

    /**
     * Set all users for department, when we are adding new department.
     *
     * @param Request $request
     * @param         $department_id
     */
    public static function setUsers(Request $request, $department_id)
    {

        if ( ! empty($request->users) && ! empty($department_id)) {
            self::$department_id = $department_id;
            $users               = collect(explode(',', $request->users));

            $users->each(function ($id){
                DepartmentRelationship::create([
                  'user_id'       => $id,
                  'department_id' => self::$department_id
                ]);
            });

        }
    }

    /**
     * Delete or add user for department
     *
     * @param Request $request
     *
     * @return string
     */
    public function department(Request $request)
    {
        if ( ! empty($request->user_id) && ! empty($request->department)) {

            $empty = DepartmentRelationship::where(['user_id' => $request->user_id, 'department_id' => $request->department])->get();

            if ($empty->isEmpty()) {
                DepartmentRelationship::create([
                  'user_id'       => $request->user_id,
                  'department_id' => $request->department
                ]);

                return 'created';
            } else {
                DepartmentRelationship::where([
                  'user_id'       => $request->user_id,
                  'department_id' => $request->department
                ])->delete();

                return 'deleted';
            }
        }
    }
}
