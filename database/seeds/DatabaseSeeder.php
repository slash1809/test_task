<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        factory(\App\User::class, 15)->create();
        factory(\App\Department::class, 15)->create();
    }
}
