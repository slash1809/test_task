<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          [
            'name'              => 'admin',
            'email'             => 'admin@test.loc',
            'email_verified_at' => now(),
            'created_at'        => now(),
            'password'          => bcrypt('password'), // password,
          ]
        ]);
    }
}
