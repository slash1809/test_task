<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Department;
use Faker\Generator as Faker;

$factory->define(Department::class, function (Faker $faker){
    return [
      'name'        => $faker->name,
      'description' => $faker->sentence(rand(5, 10)),
      'logo'        => 'app/logo/avatar.jpg'
    ];
});
