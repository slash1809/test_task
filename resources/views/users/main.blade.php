@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <vc-users :users='@json( $users )'></vc-users>
        </div>
    </div>
@endsection