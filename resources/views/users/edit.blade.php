@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">
                <h3>User</h3></div>
                @if( !empty( $user ))
                    <vc-user :user='@json($user)'></vc-user>
                @else
                    <vc-user :user="{}"></vc-user>
                @endif
            </div>
        </div>
    </div>
@endsection
