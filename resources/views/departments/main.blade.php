@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <vc-departments :departments='@json( $departments )'></vc-departments>
        </div>
    </div>
@endsection