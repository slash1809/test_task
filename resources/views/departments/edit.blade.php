@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width: 100%;">
                <div class="card-header">
                    <h3>Department</h3>
                </div>
                @if( !empty( $department ) )
                    <vc-department :department='@json($department)'></vc-department>
                @else
                    <vc-department :department='{}'></vc-department>
                @endif
            </div>
        </div>
    </div>
@endsection
