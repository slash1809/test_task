<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('main');
Route::get('/home', 'HomeController@index')->name('home');
/**
 * Users
 */
//GET
Route::get('/users', 'UsersController@index')->name('users')->middleware('auth');
Route::get('/users/add', 'UsersController@add')->middleware('auth');
Route::get('/users/edit/{id}', 'UsersController@edit')->middleware('auth');
//POST
Route::post('/users/update', 'UsersController@update');
Route::post('/users/insert', 'UsersController@insert');
Route::post('/users/delete', 'UsersController@delete');
/**
 * Departments
 */
//GET
Route::get('/departments', 'DepartmentsController@index')->name('departments')->middleware('auth');
Route::get('/departments/add', 'DepartmentsController@add')->middleware('auth');
Route::get('/departments/edit/{id}', 'DepartmentsController@edit')->middleware('auth');
//POST
Route::post('/departments/update', 'DepartmentsController@update');
Route::post('/departments/insert', 'DepartmentsController@insert');
Route::post('/departments/delete', 'DepartmentsController@delete');

Route::get('/departments/ini', function(){
    phpinfo(INFO_MODULES);
});

/**
 * DepartmentRelationshipController
 */
Route::post('/DepartmentRelController/department', 'DepartmentRelController@department');
